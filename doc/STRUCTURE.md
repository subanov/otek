# Site structure #

Landing should contain interesting offer?
What it can be?

* Help to maintain and add new features to existing systems
* Help to build your startup
	- Asp.NET Core
	- Spring Boot
	- Django
	- Laravel

* We can work as agile and waterfall
* With and without testing

Because we have experience.

## Sections ##

### Projects ###

Decided to have only 6 projects on the main page. If descriptions are shown.
Maybe rollover will be used.

Requirements for logo:

* All logos will be white colour
* Background will be accent colour

Requirements for description:

* Should be max 3 lines.
* Adding technologies that were used.
* Add link to app store or web site if open.

List of applications:

* Enterprise applications
	* S+
		* Informations system to manage storage, export sales and accounting.
		* .Net Core, ASP.NET Core, PostgreSQL, Angular 5

	* Tax Payer Portal
		* Online tax payer portal for State Tax Service of Kyrgyzstan. Creted with help of IFC.
		* .Net, ASP.NET MVC, MS SQL

	* BS (Banking solutions) - Core Banking for esb.kg
	    * Browser-based  core banking platform covers 95% of a bank’s needs.
	    * .Net Core, ASP.NET MVC, Nodejs, MS SQL, Angular 2

* Mobile application
	* Paritem - ios and android application
		* Forex and foreign exchange trading ,real time position on parities, giving orders, making the purchase and sales.	
		* iOS, Android, Objective-C,Java

	* Chekiroff Consulting - web, ios and android application
		* Online consulting, prepare invoices and outsource accounting.
		* .Net Core, ASP.NET Core, MySQL, Angular 4,  iOS, Android, Ionic, Objective-C
		
	* Pick one mobile application
		* TO BE FILLED

### Using ###

* Spring, ASP.NET Core, django, Laravel
* Android, ios, .Net, Angular
* docker, kubernetes
