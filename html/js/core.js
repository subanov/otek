$(document).ready(function(){

  new WOW().init();

  let typed = new Typed('#typed', {
    stringsElement: '#typed-strings',
    typeSpeed: 60,
    backSpeed: 30,
    backDelay: 1000,
    startDelay: 1000,
    loop: true
  });

  $('.team__slider').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    arrows: true,
    dots: false,
    responsive: [
    {
      breakpoint: 1200,
        settings: {
          slidesToShow: 3,
        }
      },
    {
      breakpoint: 992,
        settings: {
          slidesToShow: 2,
        }
      },
    {
      breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },      

      ]   
  });


})
